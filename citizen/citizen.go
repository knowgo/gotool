package citizen

import "strconv"

var weight = [17]int{7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2}
var valid_value = [11]byte{'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'}

// 效验18位身份证
func IsValidCitizenNo18(idCardNum string) bool {
	citizenNo18 := []byte(idCardNum)
	nLen := len(citizenNo18)
	if nLen != 18 {
		return false
	}

	nSum := 0
	for i := 0; i < nLen-1; i++ {
		n, _ := strconv.Atoi(string((citizenNo18)[i]))
		nSum += n * weight[i]
	}
	mod := nSum % 11
	return valid_value[mod] == citizenNo18[17]
	// if valid_value[mod] == (*citizenNo18)[17] {
	// 	return true
	// }
	// return false
}
