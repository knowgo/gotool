package gotool

import (
	"fmt"
	"math/big"
	"net"
)

func IPNtoA(ip int64) string {
	return fmt.Sprintf("%d.%d.%d.%d",
		byte(ip>>24), byte(ip>>16), byte(ip>>8), byte(ip))
}

func IPAtoN(ipStr string) int64 {
	ret := big.NewInt(0)
	ip := net.ParseIP(ipStr).To4()
	if ip == nil {
		return 0
	}
	ret.SetBytes(ip)
	return ret.Int64()
}
