package gotool

func IfElse[T any](cond bool, valueCondIsTrue, valueElse T) T {
	if cond {
		return valueCondIsTrue
	}
	return valueElse
}
