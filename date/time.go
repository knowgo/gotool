package date

import (
	"fmt"
	"time"
)

func IsDateEqual(time1, time2 time.Time) bool {
	return time1.Year() == time2.Year() && time1.Month() == time2.Month() && time1.Day() == time2.Day()
}

// func MakeDatePath(date time.Time) string {
// 	return date.Format("2006/01-02")
// }

// SetTime 保持年月日不变
func SetTime(tm time.Time, hour, minute, second int) time.Time {
	// tmstr := fmt.Sprintf("%s %02d:%02d:%02d", tm.Format("2006-01-02"), hour, minute, second)
	// tm, _ = time.ParseInLocation("2006-01-02 15:04:05", tmstr, time.Local)
	return time.Date(tm.Year(), tm.Month(), tm.Day(),
		hour, minute, second, 0, time.Local)
	// return tm
}

const (
	_minute = 60
	_hour   = 3600
	_day    = 24 * _hour
	_week   = 7 * _day
	_month  = 30 * _day
	_year   = 12 * _month
)

// func HumanizeTime(t time.Time, withSeconds ...bool) string {
// 	// time.Since(t).Hours()
// 	y, m, d := time.Now().Date()
// 	date := time.Date(y, m, d, 0, 0, 0, 0, time.Local)
// 	diff := int64(date.Sub(t).Seconds())
// 	withSecond := false
// 	if len(withSeconds) > 0 {
// 		withSecond = withSeconds[0]
// 	}
// 	// diff := int64(time.Since(t).Seconds())
// 	if diff > _day || diff < -4*_day {
// 		if withSecond {
// 			return t.Format("2006-01-02 15:04:05")
// 		}
// 		return t.Format("2006-01-02 15:04")
// 	}
// 	timeStr := ""
// 	if withSecond {
// 		timeStr = t.Format("15:04:05")
// 	} else {
// 		timeStr = t.Format("15:04")
// 	}
// 	if diff > 0 {
// 		return fmt.Sprintf("昨天 %s", timeStr)
// 	}
// 	if diff > -_day {
// 		return fmt.Sprintf("今天 %s", timeStr)
// 	}
// 	if diff > -2*_day {
// 		return fmt.Sprintf("明天 %s", timeStr)
// 	}
// 	return fmt.Sprintf("后天 %s", timeStr)
// }

func HumanizeTime(t time.Time) string {
	diff := int64(time.Since(t).Seconds())
	if diff > _day {
		return t.Format("2006-01-02 15:04:05")
	}
	if diff > _hour {
		return fmt.Sprintf("%.0f 小时前", float64(diff/_hour))
	}
	if diff > _minute {
		return fmt.Sprintf("%.0f 分钟前", float64(diff/_minute))
	}
	if diff >= 0 {
		return "刚刚" // fmt.Sprintf("%.0f 分钟前", float64(diff/minute))
	}
	if diff > -_minute {
		return "马上"
	}
	if diff > -_hour {
		return fmt.Sprintf("%.0f 分钟后", float64(-diff/_minute))
	}
	if diff > -_day {
		return fmt.Sprintf("%.0f 小时后", float64(-diff/_hour))
	}
	return t.Format("2006-01-02 15:04:05")
}

func HumanizeDatetime(t time.Time) string {
	// tv := t.Local().Unix()
	// now := time.Now().Unix()
	diff := int64(time.Since(t).Seconds())
	// fmt.Println("now ", now, time.Now().Format(time.RFC1123), time.Now().Location().String())
	// fmt.Println("time", tv, t.Format(time.RFC1123), t.Location().String())
	// // diff := now - tv
	// fmt.Println("diff", now-tv)
	if diff > 2*_year {
		return t.Format("2006-01-02 15:04:05")
	}
	if diff > _year {
		return fmt.Sprintf("%.0f 年前", float64(diff/_year))
	}
	if diff > _month {
		return fmt.Sprintf("%.0f 月前", float64(diff/_month))
	}
	if diff > _week {
		return fmt.Sprintf("%.0f 周前", float64(diff/_week))
	}
	if diff > _day {
		return fmt.Sprintf("%.0f 天前", float64(diff/_day))
	}
	if diff > _hour {
		return fmt.Sprintf("%.0f 小时前", float64(diff/_hour))
	}
	if diff > _minute {
		return fmt.Sprintf("%.0f 分钟前", float64(diff/_minute))
	}
	if diff >= 0 {
		return "刚刚" // fmt.Sprintf("%.0f 分钟前", float64(diff/minute))
	}
	if diff > -_minute {
		return "马上"
	}
	if diff > -_hour {
		return fmt.Sprintf("%.0f 分钟后", float64(-diff/_minute))
	}
	if diff > -_day {
		return fmt.Sprintf("%.0f 小时后", float64(-diff/_hour))
	}
	if diff > -_week {
		return fmt.Sprintf("%.0f 天后", float64(-diff/_day))
	}
	if diff > -_month {
		return fmt.Sprintf("%.0f 周后", float64(-diff/_week))
	}
	if diff > -_year {
		return fmt.Sprintf("%.0f 月后", float64(-diff/_month))
	}
	if diff > -2*_year {
		return fmt.Sprintf("%.0f 年后", float64(-diff/_year))
	}
	return t.Format("2006-01-02 15:04:05")
}
