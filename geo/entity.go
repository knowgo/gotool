package geo

import "strconv"

// Point 经纬度
type Point struct {
	Lat float64
	Lng float64
}

// ParsePoint 解析字符串为经纬度参数
func ParsePoint(lng, lat string) (*Point, error) {
	var err error
	var p Point
	p.Lng, err = strconv.ParseFloat(lng, 64)
	if err != nil {
		return nil, err
	}
	p.Lat, err = strconv.ParseFloat(lat, 64)
	if err != nil {
		return nil, err
	}
	return &p, nil
}

// Polygon 多边形
type Polygon struct {
	Coordinates []*Point
	Itterations int
}
