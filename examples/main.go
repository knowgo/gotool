package main

import (
	"fmt"
	"time"

	"gitee.com/knowgo/gotool/idc"
	"gitee.com/knowgo/gotool/tree"
)

type A[T comparable] interface {
	Id() T
}

type AU[T comparable] struct {
	N A[T]
}

func (a1 *AU[T]) Use(a A[T]) {
	a1.N = a
	fmt.Println(a.Id())
}

type AI struct {
}

func (a *AI) Id() int64 {
	return 100
}

type Item struct {
}

func (Item) KeyID() int64       { return 100 }
func (Item) ParentKeyID() int64 { return 1 }

func main() {
	tr := tree.NewTree[int64](func(parent, child *Item) {
		// itemDiv.Parent = c
		// 	insertPos := -1
		// 	if c.Children != nil {
		// 		for n, child := range c.Children {
		// 			if child.Code > itemDiv.Code {
		// 				insertPos = n
		// 				break
		// 			}
		// 		}
		// 	}
		// 	if insertPos == -1 {
		// 		c.Children = append(c.Children, itemDiv)
		// 	} else {
		// 		rear := append([]*ClassInfo{itemDiv}, c.Children[insertPos:]...)
		// 		c.Children = append(c.Children[:insertPos], rear...)
		// 	}
		// 	return nil
	})
	tr.AddItem(&Item{}, tree.SKOReplace)
	// tr.GetNode()

	var au = &AU[int64]{}
	au.Use(&AI{})

	idc.Init(0)
	id := idc.Next()
	tm := idc.Parse(id)
	fmt.Println(tm.Format(time.RFC3339))
}
