package crypto

import (
	"crypto/md5"
	"fmt"
	"strings"
)

//MD5Str MD5 编码
func MD5Str(str string) string {
	data := []byte(str)
	// has := md5.Sum(data)
	return MD5(data)
}

// MD5It MD5 编码
func MD5(data []byte) string {
	has := md5.Sum(data)
	return fmt.Sprintf("%x", has)
}

// MD5s MD5 加密多个字符串，以 : 分割
func MD5s(strs ...string) string {
	str := strings.Join(strs, ":")
	return MD5Str(str)
}

// MD5f 格式化后再进行 MD5 编码
func MD5f(format string, args ...interface{}) string {
	return MD5Str(fmt.Sprintf(format, args...))
}

func MD5Int64(n int64) string {
	return MD5Str(fmt.Sprintf("%d", n))
}
