package crypto

import (
	"time"
)

// 使用编译参数设置
var salt = "password"

func EncodePassword(id int64, password string) string {
	return MD5f("%s:%s:%d", salt, password, id)
}

func RandomPassword(id int64) string {
	return MD5f("%d:abc-password:%d", id, time.Now().UnixNano())[:6]
}

// func RandomPassword(id int64) string {
// 	return EncodePassword(id, fmt.Sprintf("%d:abc-password:%d", id, time.Now().UnixNano()))
// }

// func DefaultPassword(id int64) string {
// 	return EncodePassword(id, "123456")
// }
