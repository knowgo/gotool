package errs

import (
	"errors"
	"reflect"
)

func Unwrap(err error, target interface{}) error {
	if target == nil {
		return nil
		// panic("errors: target cannot be nil")
	}
	val := reflect.ValueOf(target)
	typ := val.Type()
	if typ.Kind() != reflect.Ptr || val.IsNil() {
		return nil
		// panic("errors: target must be a non-nil pointer")
	}
	targetType := typ.Elem()
	if targetType.Kind() != reflect.Interface && !targetType.Implements(errorType) {
		return nil
		// panic("errors: *target must be interface or implement error")
	}
	for err != nil {
		if reflect.TypeOf(err).AssignableTo(targetType) {
			val.Elem().Set(reflect.ValueOf(err))
			return err
		}
		if x, ok := err.(interface{ As(interface{}) bool }); ok && x.As(target) {
			return err
		}
		err = errors.Unwrap(err)
	}
	return nil
}

var errorType = reflect.TypeOf((*error)(nil)).Elem()
