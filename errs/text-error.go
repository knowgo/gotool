package errs

import "fmt"

type textError struct {
	msg string
}

func (u *textError) Error() string {
	return u.msg
}

func NewTextError(format string, a ...interface{}) error {
	return &textError{
		msg: fmt.Sprintf(format, a...),
	}
}

var errText *textError

func ParseTextError(err error, defaultText string, a ...interface{}) error {
	if er := Unwrap(err, &errText); er != nil {
		return er
	}
	return fmt.Errorf(defaultText, a...)
}

func UnwrapTextError(err error) error {
	return Unwrap(err, &errText)
}
