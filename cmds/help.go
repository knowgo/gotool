package cmds

import (
	"flag"
	"fmt"
	"os"
)

var help = Command{
	Cmd:         "help",
	Description: "帮助说明",
	Execute: func(commands *Commands) {
		if len(os.Args) >= 3 {
			subCommand := os.Args[2]
			cmd, ok := commands.cmds[subCommand]
			if ok {
				flag := NewFlagSet("help", flag.PanicOnError)
				cmd.InitFlag(flag)
				flag.Usage()
				return
			}
			fmt.Printf("Sub command [%s] is not supported\n", subCommand)
		}
		commands.Usage()
	},
}
