package cmds

type Command struct {
	Cmd         string
	Description string
	InitFlag    func(flag *FlagSet)
	Execute     func(commands *Commands)
	flag        *FlagSet
}
