package idc

import "time"

var node *Node

func Init(nodeIndex int64) (err error) {
	node, err = NewNode(nodeIndex, NodeBits)
	return err
}

func Next() int64 {
	return node.Generate()
}

func Parse(id int64) time.Time {
	millisecond := node.ParseTime(id)
	return time.Unix(millisecond/1000, 0)
}

// ParseDatePathFromID 返回 UTC 时间生成目录
func ParseDatePathFromID(id int64) string {
	date := Parse(id)
	return date.Format("2006/01-02")
}
